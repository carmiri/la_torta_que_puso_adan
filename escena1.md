#Escena 1
UN SAINETE O ASTRAKÁN DONDE EN SUBIDOS COLORES SE LES  MUESTRA A LOS LECTORES LA TORTA QUE PUSO ADÁN
El drama pasa en el cielo y en los tiempos patriarcales en que Adán era un polluelo y el mundo estaba en pañales. 
Al levantarse el telón, es San Miguel quien lo sube; llega Dios en una nube y así empieza la cuestión.
DIOS: Hecha la Tierra y el Mar y el crepúsculo y la aurora,me parece que ya es horade acostarme a descansar.
SAN MIGUEL: ¿Terminasteis el Edén?
DIOS: Hombre, claro, por supuesto,y aunque peque de inmodesto.me parece que está bien.Es sin duda lo mejor de cuanto hasta hoy he creado:tiene aire acondicionado y un río en technicolor. Y como el clima lo favorece todo allí crece que es un primor: se dan auyamas y unas papotas de este color.
SAN MIGUEL: A propósito, Señor,empeñado en sostener hoy con vos una entrevista,por aquí estuvo el nudista, que fabricasteis ayer.
DIOS: ¿Nudista? ... Debe haber alguna equivocación; yo ayer hice el cigarrón, el picure y el cochino,pero ninguno anda chino; todos tienen pantalón.
SAN MIGUEL: Señor, olvidáis a Adán,el animal de dos patas; el que vive entre las matas, como si fuera Tarzán.
DIOS: ¡Ya recuerdo! ...El ejemplar que fabriqué con pantano, y a quien el nombre de humano, le di por disimular. La intención que tuve yo, fue fabricar un cacharro, pero estaba malo el barro, y eso fue lo que salió.
SAN MIGUEL: Y bien, ¿hablaréis con él?
DIOS: Llamádmelo, por favor.
SAN MIGUEL: ¡Atención, operador! (at the telePhone) Conecte con el Vergel. y avísele al tercio aquel, que lo llama el Director.
OPERADOR: Estés en tierra o en mar, deja Adán, cuanto te ate, y acomódate en el bate, que el Viejo te quiere hablar! 
